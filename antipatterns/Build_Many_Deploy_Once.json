{
  "antiPatterns": [
    {
      "name": "Build Many, Deploy Once",
      "aliases": [
        ""
      ],
      "description": "For each stage of deployment pipeline, a software is built from scratch instead of reusing a build that has already taken place.",
      "context": "",
      "detection": "More than one task in pipeline configuration files contain build-related commands.",
      "consequences": "Duration of pipeline runs are increased with no gain. Errors might stem from different stages using different build artifacts. Maintainability of a pipeline is reduced. Amplifies the effect of other anti-patterns. Feedback from pipeline are delayed.",
      "cause": "Oversight or lack of knowledge in pipeline organization. It might also be the case that different stages require different builds, but this is also a problem of development as maintaining multiple builds makes it harder to maintain product code.",
      "solution": "Have single build task, and pass build artifacts to subsequent stages.",
      "example": "Test and deployment stages build their binaries from scratch.",
      "sources": [
        "@article{Duvall2006a, author = {Duvall, Paul}, journal = {DeveloperWorks},pages = {1--11},title = {{Automation for the people: Remove the smell from your build scripts}},url = {http: //www.ibm.com/developerworks/java/library/j-ap10106/index.html},year = {2006}}",
        "@book{humbleBook, archivePrefix = {arXiv}, arxivId = {arXiv:1011.1669v3}, author = {Humble, Jezz and Farley, David}, booktitle = {Continuous delivery}, doi = {10.1007/s13398-014-0173-7.2}, edition = {1st}, eprint = {arXiv:1011.1669v3}, isbn = {978-0-321-60191-9}, issn = {1098-6596}, pages = {497}, pmid = {25246403}, publisher = {Addison-Wesley Professional}, title = {{Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation}}, year = {2010}}",
        "@article{Zampetti2020b, abstract = {Continuous Integration (CI) has been claimed to introduce several benefits in software development, including high software quality and reliability. However, recent work pointed out challenges, barriers and bad practices characterizing its adoption. This paper empirically investigates what are the bad practices experienced by developers applying CI. The investigation has been conducted by leveraging semi-structured interviews of 13 experts and mining more than 2,300 Stack Overflow posts. As a result, we compiled a catalog of 79 CI bad smells belonging to 7 categories related to different dimensions of a CI pipeline management and process. We have also investigated the perceived importance of the identified bad smells through a survey involving 26 professional developers, and discussed how the results of our study relate to existing knowledge about CI bad practices. Whilst some results, such as the poor usage of branches, confirm existing literature, the study also highlights uncovered bad practices, e.g., related to static analysis tools or the abuse of shell scripts, and contradict knowledge from existing literature, e.g., about avoiding nightly builds. We discuss the implications of our catalog of CI bad smells for (i) practitioners, e.g., favor specific, portable tools over hacking, and do not ignore nor hide build failures, (ii) educators, e.g., teach CI culture, not just technology, and teach CI by providing examples of what not to do, and (iii) researchers, e.g., developing support for failure analysis, as well as automated CI bad smell detectors.}, author = {Zampetti, Fiorella and Vassallo, Carmine and Panichella, Sebastiano and Canfora, Gerardo and Gall, Harald and {Di Penta}, Massimiliano}, booktitle = {Empirical Software Engineering}, doi = {10.1007/s10664-019-09785-8}, file = {:home/ulfet/.var/app/com.elsevier.MendeleyDesktop/data/data/Mendeley Ltd./Mendeley Desktop/Downloaded/Zampetti et al. - 2020 - An empirical characterization of bad practices in continuous integration(3).pdf:pdf}, isbn = {1066401909785}, issn = {15737616}, keywords = {Bad practices,Continuous integration,Empirical study,Interview,Survey}, number = {2}, pages = {1095--1135}, publisher = {Empirical Software Engineering}, title = {{An empirical characterization of bad practices in continuous integration}}, volume = {25}, year = {2020} }"
      ],
      "tags": [
        "DRY Principle",
        "Fair Use of Shared Resources"
      ],
      "relatedItems": [
        {
          "relation": "relates",
          "name": "Insufficient Infrastructure"
        },
        {
          "relation": "relates",
          "name": "Long Build Times"
        },
        {
          "relation": "relates",
          "name": "Overreliance on CD Build"
        }
      ]
    }
  ]
}
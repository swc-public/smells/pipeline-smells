[![Website](https://img.shields.io/website/https/swc-public.pages.rwth-aachen.de/smells/pipe-smells?down_message=offline&up_message=online)](https://swc-public.pages.rwth-aachen.de/smells/pipeline-smells/)
[![pipeline status](https://git.rwth-aachen.de/swc-public/smells/pipeline-smells/badges/master/pipeline.svg)](https://git.rwth-aachen.de/swc-public/smells/pipeline-smells/commits/master)
[![Contributions welcome](https://img.shields.io/badge/contributions-welcome-orange.svg)](https://git.rwth-aachen.de/swc-public/smells/pipeline-smells/blob/master/CONTRIBUTING.md)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)
# Code Smells and Antipatterns

This repository should serve as a knowledge base for service-based antipatterns and overall bad smells.

It was first developed by Bogner et al., and is now extended to provide information on Code Smells, which should later be translated to the domain of Enterprise Architecture.

The repository has been created
1. to collect and browse antipatterns in a structured and uniform way (JSON format)
2. to give researchers and practitioners an easy way to access the collection and contribute their antipatterns
3. to version the results

Available under [https://swc-public.pages.rwth-aachen.de/smells/pipeline-smells/](https://swc-public.pages.rwth-aachen.de/smells/pipeline-smells/)

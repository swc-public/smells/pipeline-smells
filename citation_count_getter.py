# from scholarly import scholarly, ProxyGenerator
import os
import glob
import json
import bibtexparser
import datetime
from tqdm import tqdm
import time
from bs4 import BeautifulSoup
import requests

_CURRENT_FOLDER = os.getcwd()
_FOLDER_PATH = _CURRENT_FOLDER + '/antipatterns'
_EXTENSION = 'json'
_TARGET_PATH = _CURRENT_FOLDER + '/assets/sources.json'

_BASE_URL = 'http://api.scraperapi.com/?api_key=<API_KEY>&url='
_SCHOLAR_URL = 'https://scholar.google.com/scholar?q='


def generate_url(title):
    newtitle = title.replace(' ', '+')
    url = f'{_BASE_URL}{_SCHOLAR_URL}{newtitle}'
    return url


def my_google_scholar_parser(fc):
    soup = BeautifulSoup(fc, 'html.parser')
    # find body 
    body_content = soup.find(id='gs_bdy')
    # find entries
    results = body_content.find(id='gs_res_ccl_mid')
    # find entries
    entries = body_content.findAll("div", {"class": "gs_r"})
    # first record
    first_record = entries[0]
    content = first_record.findAll("div", {"class": "gs_ri"})[0]
    cite_row = content.findAll("div", {"class": "gs_fl"})[0]
    #
    cite_row = str(cite_row)
    start_text = 'Cited by '
    end_text = '</a>'
    #
    start = cite_row.find(start_text)
    end = start + cite_row[start:].find(end_text)
    #
    count = cite_row[start+len(start_text):end]
    #
    count_int = int(count)
    return count_int


def get_all_pairs(folderpath, extension):
    
    full_paths = glob.glob(_FOLDER_PATH + '/' + '*.' + extension)

    pairs = []
    for full_path in full_paths:
        print(full_path)
        with open(full_path) as json_file:
            file_content = json.load(json_file)
            citations = file_content['antiPatterns'][0]['sources']

            if citations is None or citations == []:
                continue

            for citation in citations:
                try:
                    formatted_citation = bibtexparser.loads(citation).entries[0]
                except:
                    continue
                title = formatted_citation['title'].strip('{').strip('}')
                citation_key = formatted_citation['ID']
                # formatted_citation = json.loads(citation)
                # print('  ', citation_key, title)
                pairs.append((citation_key, title))
    return pairs


def main():
    pairs = get_all_pairs(_FOLDER_PATH, _EXTENSION)
    print('All pairs appended:', len(pairs))

    pairs = list(set(pairs))
    print("Pairs unique ones:", len(pairs))

    responses_folder = 'responses'
    if not os.path.exists(responses_folder):
        os.makedirs(responses_folder)

    # fetch all responses
    key_count_pairs = []
    start_time = datetime.datetime.now()
    for (citation_key, title) in tqdm(pairs):
        try:
            url = generate_url(title)
            response = requests.get(url)
            fc = response.text

            response_file_name = responses_folder + '/' + title + '.html'
            with open(response_file_name, 'w') as fp:
                fp.write(fc)
                fp.close()

            citation_count = my_google_scholar_parser(fc)
        except:
            citation_count = 0

        print('  ', citation_key, citation_count)
        key_count_pairs.append( (citation_key, citation_count) )
    end_time = datetime.datetime.now()

    entries = [{"citeKey": key_count_pair[0], "citedBy": key_count_pair[1]} for key_count_pair in key_count_pairs]
    entries_json = json.dumps({'sources': entries}, indent=2)
    # print(entries_json)

    with open(_TARGET_PATH, 'w') as fp:
      fp.write(entries_json)

    elapsed_time = (end_time - start_time).total_seconds()
    print(f'Time Elapsed: {elapsed_time} seconds, {len(entries)} citations')

main()

